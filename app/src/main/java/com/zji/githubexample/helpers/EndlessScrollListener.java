package com.zji.githubexample.helpers;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;

/**
 * Created by zji on 03.05.17.
 */
public abstract class EndlessScrollListener extends OnScrollListener {
    private int mPageSize;

    public EndlessScrollListener(int pPageSize) {
        mPageSize = pPageSize;
    }

    @Override
    public void onScrolled(RecyclerView pRecyclerView, int pDx, int pDy) {
        super.onScrolled(pRecyclerView, pDx, pDy);

        if (pDy > 0) {
            LinearLayoutManager lm = (LinearLayoutManager) pRecyclerView.getLayoutManager();
            int visibleItemCount = lm.getChildCount();
            int totalItemCount = pRecyclerView.getAdapter().getItemCount();
            int pastVisiblesItems = lm.findFirstVisibleItemPosition();
            int currentPage = totalItemCount / mPageSize;

            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                onLoadMore(++currentPage, totalItemCount);
        }
    }

    public abstract void onLoadMore(int page, int totalItemsCount);

}
