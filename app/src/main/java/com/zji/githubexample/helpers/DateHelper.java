package com.zji.githubexample.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by zji on 03.05.17.
 */
public class DateHelper {
    public static String formatDate(Date date, String pattern) {
        SimpleDateFormat dt = new SimpleDateFormat(pattern);
        return dt.format(date);
    }
}
