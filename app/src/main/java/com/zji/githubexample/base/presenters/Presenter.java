package com.zji.githubexample.base.presenters;

/**
 * Created by zji on 03.05.17.
 */
public interface Presenter<T> {
    void onViewAttached(T view);

    void onViewDetached();

    void onDestroyed();
}
