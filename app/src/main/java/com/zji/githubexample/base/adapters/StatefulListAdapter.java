package com.zji.githubexample.base.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jakewharton.rxrelay2.PublishRelay;

/**
 * Created by zji on 03.05.17.
 */
public abstract class StatefulListAdapter<T, H extends BaseViewHolder<T>> extends StatefulAdapter<T, H> {
    public PublishRelay<T> onItemClick = PublishRelay.create();

    protected abstract H getViewHolder(View pView);

    protected abstract int getTemplate();

    @Override
    public H onCreateViewHolder(ViewGroup pParent, int pViewType) {
        final View v = LayoutInflater.from(pParent.getContext())
                .inflate(getTemplate(), pParent, false);
        return getViewHolder(v);
    }
}
