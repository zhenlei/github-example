package com.zji.githubexample.base.presenters;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by zji on 03.05.17.
 */
public abstract class BasePresenterActivity<P extends Presenter<V>, V> extends AppCompatActivity {
    public P mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLoader();
    }

    protected <D> Loader<D> initLoader(int id, LoaderManager.LoaderCallbacks<D> callback) {
        return getSupportLoaderManager().initLoader(id, null, callback);
    }

    private void initLoader() {
        initLoader(getLoaderId(), new LoaderManager.LoaderCallbacks<P>() {
            @Override
            public final Loader<P> onCreateLoader(int id, Bundle args) {
                return new PresenterLoader<P>(BasePresenterActivity.this, getPresenterFactory());
            }

            @Override
            public final void onLoadFinished(Loader<P> loader, P presenter) {
                mPresenter = presenter;
                mPresenter.onViewAttached((V) BasePresenterActivity.this);
                onPresenterPrepared();
            }

            @Override
            public final void onLoaderReset(Loader<P> loader) {
                mPresenter = null;
                onPresenterDestroyed();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.onViewAttached((V) this);
    }

    @Override
    protected void onStop() {
        mPresenter.onViewDetached();
        super.onStop();
    }

    protected P getPresenter() {
        return mPresenter;
    }

    protected abstract PresenterFactory<P> getPresenterFactory();

    protected abstract void onPresenterPrepared();

    protected abstract int getLoaderId();

    protected abstract void onPresenterDestroyed();

}
