package com.zji.githubexample.base.presenters;

import android.content.Context;
import android.support.v4.content.Loader;

/**
 * Created by zji on 03.05.17.
 */
public class PresenterLoader<T extends Presenter> extends Loader<T> {
    private final PresenterFactory<T> presenterFactory;
    private T presenter;

    public PresenterLoader(Context context, PresenterFactory<T> factory) {
        super(context);
        presenterFactory = factory;
    }

    @Override
    protected void onStartLoading() {
        if (presenter != null) {
            deliverResult(presenter);
            return;
        }

        forceLoad();
    }

    @Override
    protected void onForceLoad() {
        presenter = presenterFactory.create();
        deliverResult(presenter);
    }

    @Override
    public void deliverResult(T data) {
        super.deliverResult(data);
    }

    @Override
    protected void onStopLoading() {
    }

    @Override
    protected void onReset() {
        if (presenter != null) {
            presenter.onDestroyed();
            presenter = null;
        }
    }
}
