package com.zji.githubexample.base.adapters;

import android.content.Context;
import android.support.v4.content.Loader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zji on 03.05.17.
 */
public class AdapterLoader<T> extends Loader<List<T>> {
    private List<T> mData;

    public AdapterLoader(Context pContext) {
        super(pContext);
    }

    public void setData(List<T> pData) {
        mData = pData;
    }

    @Override
    protected void onStartLoading() {
        if (mData != null) {
            deliverResult(mData);
            return;
        }

        forceLoad();
    }

    @Override
    protected void onForceLoad() {
        mData = new ArrayList<T>();
        deliverResult(mData);
    }

    @Override
    public void deliverResult(List<T> pData) {
        super.deliverResult(pData);
    }

    @Override
    protected void onStopLoading() {
    }

    @Override
    protected void onReset() {
        if (mData != null)
            mData = new ArrayList<T>();
    }
}
