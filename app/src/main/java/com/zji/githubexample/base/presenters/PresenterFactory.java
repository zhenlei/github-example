package com.zji.githubexample.base.presenters;

/**
 * Created by zji on 03.05.17.
 */
public interface PresenterFactory<T extends Presenter> {
    T create();
}