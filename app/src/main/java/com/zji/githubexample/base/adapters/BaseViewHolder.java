package com.zji.githubexample.base.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by zji on 03.05.17.
 */
public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {
    protected BaseViewHolder(View pItemView) {
        super(pItemView);
    }

    public abstract void dataBind(T pItem);
}
