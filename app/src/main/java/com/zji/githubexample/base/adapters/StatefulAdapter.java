package com.zji.githubexample.base.adapters;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.jakewharton.rxrelay2.PublishRelay;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zji on 03.05.17.
 */
public abstract class StatefulAdapter<T, H extends BaseViewHolder> extends BaseAdapter<T, H> implements LoaderManager.LoaderCallbacks<List<T>> {

    public PublishRelay<List<T>> onLoaded = PublishRelay.create();
    private AdapterLoader<T> mAdapterLoader;

    @Override
    public void appendItems(T[] pItems) {
        super.appendItems(pItems);
        if (mAdapterLoader != null)
            mAdapterLoader.setData(mRepositories);
    }

    @Override
    public void clear() {
        super.clear();
        if (mAdapterLoader != null)
            mAdapterLoader.setData(mRepositories);
    }

    @Override
    public final Loader<List<T>> onCreateLoader(int pId, Bundle pArgs) {
        mAdapterLoader = new AdapterLoader<T>(mContext);
        return mAdapterLoader;
    }

    @Override
    public final void onLoadFinished(Loader<List<T>> pLoader, List<T> pData) {
        mRepositories = pData;
        if (onLoaded != null) {
            onLoaded.accept(mRepositories);
        }
    }

    @Override
    public final void onLoaderReset(Loader<List<T>> pLoader) {
        mRepositories = new ArrayList<T>();
    }
}
