package com.zji.githubexample.base.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zji on 03.05.17.
 */
@EBean
public abstract class BaseAdapter<T, H extends BaseViewHolder> extends RecyclerView.Adapter<H> {
    protected List<T> mRepositories;

    @RootContext
    protected Context mContext;

    public BaseAdapter() {
        mRepositories = new ArrayList<T>();
    }

    public T getItem(int pIndex) {
        return mRepositories.get(pIndex);
    }

    public void appendItems(T[] pItems) {
        for (T item : pItems)
            mRepositories.add(item);
        notifyDataSetChanged();
    }

    public List<T> getItems() {
        return mRepositories;
    }

    public void clear() {
        mRepositories.clear();
        notifyDataSetChanged();
    }

    public boolean isEmpty() {
        return mRepositories.isEmpty();
    }

    @Override
    public void onBindViewHolder(H pHolder, int pPosition) {
        pHolder.dataBind(mRepositories.get(pPosition));
    }

    @Override
    public int getItemCount() {
        return mRepositories.size();
    }
}
