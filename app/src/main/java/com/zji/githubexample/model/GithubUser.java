package com.zji.githubexample.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by zji on 02.05.17.
 */
@Parcel
public class GithubUser {
    @SerializedName("login")
    private String mLogin;

    @SerializedName("id")
    private int mId;

    @SerializedName("avatar_url")
    private String mAvatarUrl;

    public String getLogin() {
        return mLogin;
    }

    public void setLogin(String pLogin) {
        mLogin = pLogin;
    }

    public int getId() {
        return mId;
    }

    public void setId(int pId) {
        mId = pId;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public void setAvatarUrl(String pAvatarUrl) {
        mAvatarUrl = pAvatarUrl;
    }
}
