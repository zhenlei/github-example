package com.zji.githubexample.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by zji on 02.05.17.
 */
@Parcel
public class GithubRepoItem {
    @SerializedName("id")
    private int mId;

    @SerializedName("name")
    private String mName;

    @SerializedName("full_name")
    private String mFullName;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("forks_count")
    private int mForksCount;

    @SerializedName("stargazers_count")
    private int mStargazersCount;

    @SerializedName("owner")
    private GithubUser mUser;

    public GithubUser getUser() {
        return mUser;
    }

    public void setUser(GithubUser pUser) {
        mUser = pUser;
    }

    public int getId() {
        return mId;
    }

    public void setId(int pIdd) {
        mId = pIdd;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String pFullName) {
        mFullName = pFullName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String pDescription) {
        mDescription = pDescription;
    }

    public int getForksCount() {
        return mForksCount;
    }

    public void setForksCount(int pForksCount) {
        mForksCount = pForksCount;
    }

    public int getStargazersCount() {
        return mStargazersCount;
    }

    public void setStargazersCount(int pStargazersCount) {
        mStargazersCount = pStargazersCount;
    }

    public String getName() {
        return mName;
    }

    public void setName(String pName) {
        mName = pName;
    }
}
