package com.zji.githubexample.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zji on 02.05.17.
 */
public class GithubRepoListResult {
    @SerializedName("total_count")
    private int mTotalCount;

    @SerializedName("items")
    private GithubRepoItem[] mItems;

    public int getTotalCount() {
        return mTotalCount;
    }

    public void setTotalCount(int pTotalCount) {
        mTotalCount = pTotalCount;
    }

    public GithubRepoItem[] getItems() {
        return mItems;
    }

    public void setItems(GithubRepoItem[] pItems) {
        mItems = pItems;
    }
}
