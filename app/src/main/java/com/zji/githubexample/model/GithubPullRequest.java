package com.zji.githubexample.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by zji on 02.05.17.
 */
public class GithubPullRequest {
    @SerializedName("id")
    private int mId;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("html_url")
    private String mHtmlUrl;

    @SerializedName("state")
    private String mState;

    @SerializedName("body")
    private String mBody;

    @SerializedName("created_at")
    private Date mCreatedAt;

    @SerializedName("user")
    private GithubUser mUser;

    public GithubUser getUser() {
        return mUser;
    }

    public void setUser(GithubUser pUser) {
        mUser = pUser;
    }

    public int getId() {
        return mId;
    }

    public void setId(int pId) {
        mId = pId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String pTitle) {
        mTitle = pTitle;
    }

    public String getHtmlUrl() {
        return mHtmlUrl;
    }

    public void setHtmlUrl(String pHtmlUrl) {
        mHtmlUrl = pHtmlUrl;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(String pBody) {
        mBody = pBody;
    }

    public Date getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(Date pCreatedAt) {
        mCreatedAt = pCreatedAt;
    }

    public String getState() {
        return mState;
    }

    public void setState(String pState) {
        mState = pState;
    }
}
