package com.zji.githubexample.feature.main;

import com.zji.githubexample.R;
import com.zji.githubexample.api.GithubService;
import com.zji.githubexample.base.presenters.Presenter;
import com.zji.githubexample.helpers.NetworkHelper;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by zji on 03.05.17.
 */
public class MainPresenter implements Presenter<MainView> {

    private MainView mView;
    private Disposable mDisposable;

    @Override
    public void onViewAttached(MainView view) {
        mView = view;
    }

    @Override
    public void onViewDetached() {
        mView = null;
    }

    @Override
    public void onDestroyed() {

    }

    public void refreshRepositories() {
        loadRepositories(1);
    }

    public void loadRepositories(int pPage) {
        if (isLoadSubscribed() == false) {

            if (!NetworkHelper.isNetworkAvailable(mView.getApplicationContext())) {
                mView.showNetworkConnectionError();
                return;
            }

            mDisposable = GithubService.getClient()
                    .getPopularRepositories(mView.getApplicationContext().getResources().getString(R.string.languague_query, "java"), pPage)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(pDisposable -> {
                        if (mView != null) {
                            mView.showLoading(true);
                        }
                    })
                    .doOnTerminate(() -> {
                        if (mView != null) {
                            mView.showLoading(false);
                        }
                    })
                    .subscribe(result -> {
                                if (mView != null) {
                                    mView.showRepositories(result.getItems());
                                }
                            },
                            e -> {
                                if (mView != null) {
                                    mView.showErrorOnLoad();
                                }
                            });
        }
    }

    private boolean isLoadSubscribed() {
        return mDisposable != null && !mDisposable.isDisposed();
    }
}
