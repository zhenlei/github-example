package com.zji.githubexample.feature.main;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.zji.githubexample.R;
import com.zji.githubexample.base.presenters.BasePresenterActivity;
import com.zji.githubexample.base.presenters.PresenterFactory;
import com.zji.githubexample.feature.details.PullRequestActivity;
import com.zji.githubexample.feature.details.PullRequestActivity_;
import com.zji.githubexample.helpers.EndlessScrollListener;
import com.zji.githubexample.model.GithubRepoItem;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.parceler.Parcels;

/**
 * Created by zji on 03.05.17.
 */
@EActivity(R.layout.activity_main)
public class MainActivity extends BasePresenterActivity<MainPresenter, MainView> implements MainView {

    public static final int PAGE_SIZE = 30;

    @ViewById(R.id.toolbar)
    Toolbar mToolbar;

    @ViewById(R.id.empty_view)
    TextView mEmptyView;

    @ViewById(R.id.rv_repositories)
    RecyclerView mRecyclerView;

    @ViewById(R.id.refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Bean
    GithubRepoListAdapter mGithubRepoListAdapter;

    @Override
    protected PresenterFactory<MainPresenter> getPresenterFactory() {
        return new MainPresenterFactory();
    }

    @Override
    protected void onPresenterPrepared() {
        setSupportActionBar(mToolbar);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mGithubRepoListAdapter);
        mRecyclerView.addOnScrollListener(new EndlessScrollListener(PAGE_SIZE) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                getPresenter().loadRepositories(page);
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mGithubRepoListAdapter.clear();
            getPresenter().refreshRepositories();
        });

        mGithubRepoListAdapter.onLoaded.subscribe(data -> {
            if (data.isEmpty()) {
                getPresenter().refreshRepositories();
            }
        });

        initLoader(mGithubRepoListAdapter.LOADER_ID, mGithubRepoListAdapter);

        mGithubRepoListAdapter.onItemClick.subscribe(data -> {
            startActivity(new Intent(this, PullRequestActivity_.class)
                    .putExtra(PullRequestActivity.EXTRA_PULL_REQUEST, Parcels.wrap(data)));
        });
    }

    @Override
    protected int getLoaderId() {
        return 4321;
    }

    @Override
    protected void onPresenterDestroyed() {

    }

    @Override
    public void showRepositories(GithubRepoItem[] items) {
        mEmptyView.setVisibility(View.GONE);

        mGithubRepoListAdapter.appendItems(items);

        if (mGithubRepoListAdapter.isEmpty())
            showEmptyMessage();
    }

    @Override
    public void showLoading(boolean pVisible) {
        // SwipeRefreshLayout indicator does not appear when the setRefreshing(true) is called
        // before the SwipeRefreshLayout.onMeasure(). Create a runnable to solve this issue.
        mSwipeRefreshLayout.post(() -> mSwipeRefreshLayout.setRefreshing(pVisible));
    }

    @Override
    public void showNetworkConnectionError() {
        showLoading(false);
        showMessage(R.string.error_network_connection_broken);
    }

    @Override
    public void showErrorOnLoad() {
        showMessage(R.string.error_load_data);
    }

    public void showMessage(int message) {
        if (mGithubRepoListAdapter.isEmpty()) {
            mEmptyView.setVisibility(View.VISIBLE);
            mEmptyView.setText(getResources().getString(message));
        } else {
            mEmptyView.setVisibility(View.GONE);
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
        }
    }

    public void showEmptyMessage() {
        showMessage(R.string.no_results);
    }
}
