package com.zji.githubexample.feature.details;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zji.githubexample.R;
import com.zji.githubexample.base.presenters.BasePresenterActivity;
import com.zji.githubexample.base.presenters.PresenterFactory;
import com.zji.githubexample.helpers.EndlessScrollListener;
import com.zji.githubexample.model.GithubPullRequest;
import com.zji.githubexample.model.GithubRepoItem;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.parceler.Parcels;

/**
 * Created by zji on 03.05.17.
 */
@EActivity(R.layout.activity_pull_requests)
public class PullRequestActivity extends BasePresenterActivity<PullRequestPresenter, PullRequestView> implements PullRequestView {
    public static final String EXTRA_PULL_REQUEST = "com.zji.githubexample.feature.details.EXTRA_PULL_REQUEST";
    public static final int PAGE_SIZE = 30;

    @ViewById(R.id.toolbar)
    Toolbar mToolbar;

    @ViewById(R.id.empty_view)
    TextView mEmptyView;

    @ViewById(R.id.rv_pull_requests)
    RecyclerView mRecyclerView;

    @ViewById(R.id.refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @ViewById(R.id.tv_pull_request_opened)
    TextView mOpenedPullRequestCountTextView;

    @ViewById(R.id.tv_pull_request_closed)
    TextView mClosedPullRequestCountTextView;

    @ViewById(R.id.ll_count_bar_view)
    LinearLayout mCountBar;

    @Bean
    GitHubPullRequestListAdapter mGitHubPullRequestListAdapter;

    private GithubRepoItem mRepoItem;

    @Override
    protected PresenterFactory<PullRequestPresenter> getPresenterFactory() {
        return new PullRequestPresenterFactory();
    }

    @Override
    protected void onPresenterPrepared() {
        mRepoItem = Parcels.unwrap(getIntent().getParcelableExtra(EXTRA_PULL_REQUEST));

        mToolbar.setTitle(mRepoItem.getFullName());

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mGitHubPullRequestListAdapter);
        mRecyclerView.addOnScrollListener(new EndlessScrollListener(PAGE_SIZE) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                getPresenter().loadPullRequests(page);
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mGitHubPullRequestListAdapter.clear();
            getPresenter().refreshPullRequests();
        });

        mGitHubPullRequestListAdapter.onLoaded.subscribe(data -> {
            if (data.isEmpty()) {
                getPresenter().refreshPullRequests();
            } else {
                getPresenter().loadCounts(data);
            }
        });

        initLoader(mRepoItem.getId(), mGitHubPullRequestListAdapter);

        mGitHubPullRequestListAdapter.onItemClick.subscribe(data ->
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(data.getHtmlUrl()))));
    }

    @Override
    protected int getLoaderId() {
        return 2131;
    }

    @Override
    protected void onPresenterDestroyed() {

    }

    @Override
    public void showPullRequests(GithubPullRequest[] pItems) {
        mEmptyView.setVisibility(View.GONE);

        mGitHubPullRequestListAdapter.appendItems(pItems);

        if (mGitHubPullRequestListAdapter.isEmpty())
            showEmptyMessage();
        else
            getPresenter().loadCounts(mGitHubPullRequestListAdapter.getItems());

    }

    @Override
    public void showLoading(boolean pVisible) {
        // SwipeRefreshLayout indicator does not appear when the setRefreshing(true) is called
        // before the SwipeRefreshLayout.onMeasure(). Create a runnable to solve this issue.
        mSwipeRefreshLayout.post(() -> mSwipeRefreshLayout.setRefreshing(pVisible));

        mCountBar.setVisibility(pVisible ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showOpenedPullRequestCount(long pCount) {
        mOpenedPullRequestCountTextView.setText(String.valueOf(pCount));
    }

    @Override
    public void showClosedPullRequestCount(long pCount) {
        mClosedPullRequestCountTextView.setText(String.valueOf(pCount));
    }

    @Override
    public void showNetworkConnectionError() {
        showLoading(false);
        showMessage(R.string.error_network_connection_broken);
    }

    @Override
    public void showErrorOnLoad() {
        showMessage(R.string.error_load_data);
    }

    public void showMessage(int pMessage) {
        boolean isEmpty = mGitHubPullRequestListAdapter.isEmpty();
        if (isEmpty) {
            mEmptyView.setText(getResources().getString(pMessage));
            mEmptyView.setVisibility(View.VISIBLE);
        } else {
            mEmptyView.setVisibility(View.GONE);
            Toast.makeText(PullRequestActivity.this, pMessage, Toast.LENGTH_LONG).show();
        }
    }

    public void showEmptyMessage() {
        showMessage(R.string.no_results);
    }

    @Override
    public String getRepository() {
        return mRepoItem.getName();
    }

    @Override
    public String getOwner() {
        return mRepoItem.getUser().getLogin();
    }
}
