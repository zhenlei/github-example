package com.zji.githubexample.feature.main;

import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.jakewharton.rxrelay2.PublishRelay;
import com.squareup.picasso.Picasso;
import com.zji.githubexample.R;
import com.zji.githubexample.base.adapters.BaseViewHolder;
import com.zji.githubexample.model.GithubRepoItem;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by zji on 03.05.17.
 */
public class GithubRepoViewHolder extends BaseViewHolder<GithubRepoItem> {

    public PublishRelay<GithubRepoItem> mOnClick;

    public GithubRepoViewHolder(View pItemView, PublishRelay<GithubRepoItem> pOnClick) {
        super(pItemView);
        mOnClick = pOnClick;
    }

    @Override
    public void dataBind(final GithubRepoItem pItem) {
        TextView title = (TextView) itemView.findViewById(R.id.repository_title);
        title.setText(pItem.getFullName());

        String repoDescription = pItem.getDescription();

        TextView description = (TextView) itemView.findViewById(R.id.repository_description);
        description.setText(repoDescription);

        TextView username = (TextView) itemView.findViewById(R.id.user_name);
        username.setText(pItem.getUser().getLogin());

        TextView forks = (TextView) itemView.findViewById(R.id.repository_forks);
        forks.setText(String.valueOf(pItem.getForksCount()));

        TextView stars = (TextView) itemView.findViewById(R.id.repository_stars);
        stars.setText(String.valueOf(pItem.getStargazersCount()));

        CircleImageView image = (CircleImageView) itemView.findViewById(R.id.user_picture);
        Picasso.with(itemView.getContext())
                .load(Uri.parse(pItem.getUser().getAvatarUrl()))
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(image);

        itemView.setOnClickListener(view -> mOnClick.accept(pItem));
    }
}
