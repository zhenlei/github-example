package com.zji.githubexample.feature.details;

import android.view.View;

import com.zji.githubexample.R;
import com.zji.githubexample.base.adapters.StatefulListAdapter;
import com.zji.githubexample.model.GithubPullRequest;

import org.androidannotations.annotations.EBean;

/**
 * Created by zji on 03.05.17.
 */
@EBean
public class GitHubPullRequestListAdapter extends StatefulListAdapter<GithubPullRequest, GithubPullRequestViewHolder> {

    @Override
    protected GithubPullRequestViewHolder getViewHolder(View v) {
        return new GithubPullRequestViewHolder(v, onItemClick);
    }

    @Override
    protected int getTemplate() {
        return R.layout.card_pull_request;
    }
}
