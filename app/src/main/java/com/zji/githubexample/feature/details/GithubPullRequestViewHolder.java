package com.zji.githubexample.feature.details;

import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.jakewharton.rxrelay2.PublishRelay;
import com.squareup.picasso.Picasso;
import com.zji.githubexample.R;
import com.zji.githubexample.base.adapters.BaseViewHolder;
import com.zji.githubexample.helpers.DateHelper;
import com.zji.githubexample.model.GithubPullRequest;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by zji on 03.05.17.
 */
public class GithubPullRequestViewHolder extends BaseViewHolder<GithubPullRequest> {
    private PublishRelay<GithubPullRequest> onClick;

    public GithubPullRequestViewHolder(View pItemView, PublishRelay<GithubPullRequest> pOnClick) {
        super(pItemView);
        onClick = pOnClick;
    }

    @Override
    public void dataBind(final GithubPullRequest pItem) {
        TextView title = (TextView) itemView.findViewById(R.id.pull_request_title);
        title.setText(pItem.getTitle());

        TextView description = (TextView) itemView.findViewById(R.id.pull_request_description);
        description.setText(pItem.getBody());

        TextView createdAt = (TextView) itemView.findViewById(R.id.pull_request_date);
        createdAt.setText(DateHelper.formatDate(pItem.getCreatedAt(), "MM/dd/yyyy HH:mm:ss"));

        TextView username = (TextView) itemView.findViewById(R.id.pull_request_user_name);
        username.setText(pItem.getUser().getLogin());

        CircleImageView image = (CircleImageView) itemView.findViewById(R.id.pull_request_user_picture);

        Picasso.with(itemView.getContext())
                .load(Uri.parse(pItem.getUser().getAvatarUrl()))
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(image);

        itemView.setOnClickListener(view -> onClick.accept(pItem));
    }
}
