package com.zji.githubexample.feature.details;


import com.zji.githubexample.api.GithubService;
import com.zji.githubexample.base.presenters.Presenter;
import com.zji.githubexample.helpers.NetworkHelper;
import com.zji.githubexample.model.GithubPullRequest;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by zji on 03.05.17.
 */
public class PullRequestPresenter implements Presenter<PullRequestView> {

    private PullRequestView mView;
    private Disposable mDisposable;

    @Override
    public void onViewAttached(PullRequestView view) {
        mView = view;
    }

    public void refreshPullRequests() {
        loadPullRequests(1);
    }

    public void loadCounts(List<GithubPullRequest> items) {
        if (mView == null)
            throw new IllegalArgumentException("View cannot be null.");

        if (items == null) {
            mView.showOpenedPullRequestCount(0);
            mView.showClosedPullRequestCount(0);
            return;
        }

        Observable.fromIterable(items)
                .filter(x -> "open".equals(x.getState()))
                .count()
                .subscribe(result -> {
                    if (result != null) {
                        mView.showOpenedPullRequestCount(result);
                    }
                });

        Observable.fromIterable(items)
                .filter(x -> "closed".equals(x.getState()))
                .count()
                .subscribe(result -> {
                    if (mView != null) {
                        mView.showClosedPullRequestCount(result);
                    }
                });
    }

    public void loadPullRequests(int page) {
        if (isLoadSubscribed())
            mDisposable.dispose();

        if (!NetworkHelper.isNetworkAvailable(mView.getApplicationContext())) {
            mView.showNetworkConnectionError();
            return;
        }

        mDisposable = GithubService.getClient()
                .getRepositoryPullRequests(mView.getRepository(), mView.getOwner(), page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(pDisposable -> {
                    if (mView != null) {
                        mView.showLoading(true);
                    }
                })
                .doOnTerminate(() -> {
                    if (mView != null) {
                        mView.showLoading(false);
                    }
                })
                .subscribe(result -> {
                            if (mView != null) {
                                mView.showPullRequests(result);
                            }
                        },
                        e -> {
                            if (mView != null) {
                                mView.showErrorOnLoad();
                                mView.showOpenedPullRequestCount(0);
                                mView.showClosedPullRequestCount(0);
                            }
                        });
    }

    private boolean isLoadSubscribed() {
        return mDisposable != null && !mDisposable.isDisposed();
    }

    @Override
    public void onViewDetached() {
        mView = null;
    }

    @Override
    public void onDestroyed() {

    }
}
