package com.zji.githubexample.feature.main;


import com.zji.githubexample.base.presenters.PresenterFactory;

/**
 * Created by zji on 03.05.17.
 */
public class MainPresenterFactory implements PresenterFactory<MainPresenter> {
    @Override
    public MainPresenter create() {
        return new MainPresenter();
    }
}
