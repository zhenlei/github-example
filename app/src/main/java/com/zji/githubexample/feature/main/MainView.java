package com.zji.githubexample.feature.main;

import android.content.Context;

import com.zji.githubexample.model.GithubRepoItem;

/**
 * Created by zji on 03.05.17.
 */
public interface MainView {
    void showRepositories(GithubRepoItem[] pItems);

    void showLoading(boolean pVisible);

    Context getApplicationContext();

    void showNetworkConnectionError();

    void showErrorOnLoad();
}
