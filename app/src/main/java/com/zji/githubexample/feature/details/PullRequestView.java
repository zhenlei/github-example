package com.zji.githubexample.feature.details;

import android.content.Context;

import com.zji.githubexample.model.GithubPullRequest;

/**
 * Created by zji on 03.05.17.
 */
public interface PullRequestView {
    void showPullRequests(GithubPullRequest[] pItems);

    void showLoading(boolean pVisible);

    void showOpenedPullRequestCount(long pCount);

    void showClosedPullRequestCount(long pCount);

    Context getApplicationContext();

    void showNetworkConnectionError();

    void showErrorOnLoad();

    String getRepository();

    String getOwner();
}
