package com.zji.githubexample.feature.main;

import android.view.View;

import com.zji.githubexample.R;
import com.zji.githubexample.base.adapters.StatefulListAdapter;
import com.zji.githubexample.model.GithubRepoItem;

import org.androidannotations.annotations.EBean;

/**
 * Created by zji on 03.05.17.
 */
@EBean
public class GithubRepoListAdapter extends StatefulListAdapter<GithubRepoItem, GithubRepoViewHolder> {
    public final int LOADER_ID = 3001;

    @Override
    protected GithubRepoViewHolder getViewHolder(View pView) {
        return new GithubRepoViewHolder(pView, onItemClick);
    }

    @Override
    protected int getTemplate() {
        return R.layout.card_repository;
    }
}
