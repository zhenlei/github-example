package com.zji.githubexample.feature.details;


import com.zji.githubexample.base.presenters.PresenterFactory;

/**
 * Created by zji on 03.05.17.
 */
public class PullRequestPresenterFactory implements PresenterFactory<PullRequestPresenter> {
    @Override
    public PullRequestPresenter create() {
        return new PullRequestPresenter();
    }
}
