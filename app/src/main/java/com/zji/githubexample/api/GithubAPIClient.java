package com.zji.githubexample.api;

import com.zji.githubexample.model.GithubPullRequest;
import com.zji.githubexample.model.GithubRepoListResult;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by zji on 02.05.17.
 */

public interface GithubAPIClient {
    @GET("search/repositories?sort=stars")
    Observable<GithubRepoListResult> getPopularRepositories(@Query("q") String language, @Query("page") int page);

    @GET("repos/{user}/{repository}/pulls?state=all")
    Observable<GithubPullRequest[]> getRepositoryPullRequests(@Path("repository") String repository,
                                                              @Path("user") String user,
                                                              @Query("page") int page);
}
